package com.elavon.jtutorial;

public class MyClassParent {
	private String name;

	public String getName() {
		return "Parent " + name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
