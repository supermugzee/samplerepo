package com.elavon.jtutorial;

abstract class MyAbstractClass {
	int myField;
	static int myStaticField;

	void myMethod1() {
	}

	abstract void myAbstractMethod();
}