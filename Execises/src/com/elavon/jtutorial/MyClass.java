package com.elavon.jtutorial;

class MyClass extends MyClassParent {

	private int myField;
	private String name;

	// This is a default method
	void myMethod1() {
	}

	public String getName() {

		return "My Class " + name;
	}

	private void displayName() {
		System.out.println(name);
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setName(String name, String nickname) {
		this.name = name + " " + nickname;
	}

	public void setName(String name, int lastname) {
		this.name = name + " ";
	}

	MyClass() {

	}

	public static void main(String[] args) {

		MyClass class1 = new MyClass();

		class1.setName("Jay");
		System.out.println(class1.getName());
		class1.setName("Sample1");
	}

}
